const { Loads } = require("../models/Loads/Loads");
const loadUtils = require("../models/Loads/utils");
const errors = require("../errors");

const saveLoad = async (newLoad) => {
  const load = new Loads(newLoad);
  return load.save();
};

const getLoads = async ({ status, limit, offset, assigned_to, created_by }) => {
  const filters = {
    ...(assigned_to && { assigned_to }),
    ...(status && { status }),
    ...(created_by && { created_by }),
  };

  return Loads.find(filters).skip(offset).limit(limit);
};

const getActive = async ({ assigned_to, status }) => {
  const filters = {
    assigned_to,
    status: { $in: [...status] },
  };

  return Loads.findOne(filters);
};

const iterateActiveLoadState = async ({ assigned_to, status }) => {
  const filters = {
    assigned_to,
    status,
  };

  const load = await Loads.findOne(filters);
  if (!load) errors.errorLoadNotFound();

  load.state = loadUtils.getNextState(load.state);

  return load.save();
};

const getLoadById = async ({ loadId }) => {
  const filters = {
    _id: loadId,
  };

  return Loads.findOne(filters);
};

const editLoadById = async ({ loadId, created_by, ...body }) => {
  const filters = {
    ...created_by,
    _id: loadId,
  };
  const load = await Loads.findOne(filters);
  if (!load) return errors.errorLoadNotFound();

  load.name = body.name;
  load.payload = body.payload;
  load.pickup_address = body.pickup_address;
  load.delivery_address = body.delivery_address;
  load.dimensions = body.dimensions;

  return load.save();
};

const getCreatorsLoadById = async ({ loadId, created_by, status }) => {
  const filters = {
    ...created_by,
    _id: loadId,
    status,
  };

  return Loads.findOne(filters);
};

const deleteLoadById = async ({ loadId, created_by }) => {
  const filters = {
    ...created_by,
    _id: loadId,
  };

  const load = await Loads.findOne(filters);
  if (!load) return errors.errorLoadNotFound();

  return Loads.findByIdAndDelete(loadId);
};

module.exports = {
  saveLoad,
  getLoads,
  getLoadById,
  editLoadById,
  getActive,
  iterateActiveLoadState,
  deleteLoadById,
  getCreatorsLoadById,
};
