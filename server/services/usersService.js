require('dotenv').config();
const bcryptjs = require('bcryptjs');

const { User } = require('../models/Users/Users');

const getUser = async (email) => User.findOne({ email });

const deleteUser = async (userId) => User.findByIdAndDelete(userId);

const changePassword = async (userId, newPassword) => {
  const user = await User.findById(userId);

  user.password = await bcryptjs.hash(
    newPassword,
    parseInt(process.env.BCRYPT_SALT) // eslint-disable-line
  );

  return user.save();
};

module.exports = {
  getUser,
  deleteUser,
  changePassword,
};
