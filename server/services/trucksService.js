const { Truck } = require('../models/Trucks/Trucks');
const truckData = require('../models/Trucks/data');

const saveTruck = async (newTruck) => {
  const truck = new Truck(newTruck);
  return truck.save();
};

const getTrucks = async ({ userId }) => Truck.find({ created_by: userId });

const getTruckById = async (truckId) => Truck.findById(truckId);

const getTruck = async ({ assigned_to }) => {
  const filters = {
    ...(assigned_to && { assigned_to }),
  };
  return Truck.findOne(filters);
};

const changeTruckTypeById = async ({ truckId, type }) => {
  const truck = await Truck.findById(truckId);
  truck.type = type;

  return truck.save();
};

const deleteTruckById = async ({ truckId }) => Truck.findByIdAndDelete(truckId);

const assignTruckById = async ({ truckId, assigned_to }) => {
  const truck = await Truck.findById(truckId);
  truck.assigned_to = assigned_to;

  return truck.save();
};

const finishDelivery = async ({ assigned_to }) => {
  const truck = await Truck.findOne({ assigned_to });
  truck.status = truckData.statuses.IS;

  return truck.save();
};

// eslint-disable-next-line max-len
const availableTruck = async ({ status, type, assigned_to }) => Truck.findOne({ status, type, assigned_to });

module.exports = {
  saveTruck,
  getTrucks,
  getTruck,
  getTruckById,
  changeTruckTypeById,
  deleteTruckById,
  assignTruckById,
  availableTruck,
  finishDelivery,
};
