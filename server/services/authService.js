require('dotenv').config();
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { random } = require('../utils');
const usersService = require('./usersService');
const errors = require('../errors');

const { User } = require('../models/Users/Users');

const saveUser = async ({ email, password, role }) => {
  const user = new User({
    email,
    role,
    password: await bcryptjs.hash(password, parseInt(process.env.BCRYPT_SALT)), // eslint-disable-line
  });

  return user.save();
};

const getToken = async ({ email, password }) => {
  const user = await usersService.getUser(email);
  if (!user) errors.errorUserNotFound();

  const isPasswordValid = await bcryptjs.compare(
    String(password),
    String(user.password),
  );
  if (!isPasswordValid) throw new Error('Password not valid');

  const payload = {
    email: user.email,
    id: String(user._id),
    role: user.role,
  };

  const jwtToken = jwt.sign(payload, process.env.SECRET_JWT_KEY); // eslint-disable-line

  return { jwt_token: jwtToken };
};

const setNewPassword = async ({ email }) => {
  const user = await usersService.getUser(email);
  if (!user) errors.errorUserNotFound();

  const password = random(1000, 9999);
  user.password = await bcryptjs.hash(
    `${password}`,
    parseInt(process.env.BCRYPT_SALT) // eslint-disable-line
  );
  await user.save();

  return password;
};

module.exports = {
  saveUser,
  getToken,
  setNewPassword,
};
