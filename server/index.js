const express = require('express');
const morgan = require('morgan');
require('dotenv').config();
const cors = require('cors');

const mongoose = require('mongoose');
const { message } = require('./utils');

const app = express();
app.use(cors());

// app.use(function (req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header(
//     "Access-Control-Allow-Headers",
//     "Origin, X-Requested-With, Content-Type, Accept"
//   );
//   next();
// });

mongoose
  .connect(process.env.DB_CONNECT)
  .then(() => console.log('Connected DB'))
  .catch(() => app.use((req, res) => {
    res.status(500).send(message('Could not connect to database'));
  }));

const { trucksRouter } = require('./routers/trucksRouter');
const { loadsRouter } = require('./routers/loadsRouter');
const { authRouter } = require('./routers/authRouter');
const { usersRouter } = require('./routers/usersRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);
app.use('/api/users', usersRouter);
app.use('/api/auth', authRouter);

try {
  app.listen(8080);
} catch (err) {
  console.error(`Error on server startup: ${err.message}`);
}

/**
 * send status 500 with exception description
 * @param {object} err - Error message
 * @param {object} req - the request object.
 * @param {object} res - the response object.
 */
function errorHandler(err, req, res) {
  res.status(500).send(message(err));
}

// ERROR HANDLER
app.use(errorHandler);
