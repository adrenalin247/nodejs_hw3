const express = require('express');

const router = express.Router();

const { asyncWrapper } = require('../utils');
const { authMiddleware } = require('../middleware/authMiddleware');

const usersController = require('../controllers/usersController');

router.get('/me', authMiddleware, asyncWrapper(usersController.getAuthenticatedUser));
router.delete('/me', authMiddleware, asyncWrapper(usersController.deleteAuthenticatedUser));
router.patch('/me/password', authMiddleware, asyncWrapper(usersController.changeUserPassword));

module.exports = {
  usersRouter: router,
};
