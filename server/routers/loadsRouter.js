const express = require('express');

const router = express.Router();

const { asyncWrapper } = require('../utils');
const { authMiddleware } = require('../middleware/authMiddleware');

const loadsController = require('../controllers/loadsController');

router.post('/', authMiddleware, asyncWrapper(loadsController.addLoad));
router.get('/', authMiddleware, asyncWrapper(loadsController.getAllLoads));
router.get(
  '/active',
  authMiddleware,
  asyncWrapper(loadsController.getActiveLoad),
);
router.patch(
  '/active/state',
  authMiddleware,
  asyncWrapper(loadsController.iterateActiveLoadState),
);
router.get('/:id', authMiddleware, asyncWrapper(loadsController.getLoad));
router.put('/:id', authMiddleware, asyncWrapper(loadsController.editLoad));
router.delete('/:id', authMiddleware, asyncWrapper(loadsController.deleteLoad));
router.post(
  '/:id/post',
  authMiddleware,
  asyncWrapper(loadsController.postLoad),
);
router.get(
  '/:id/shipping_info',
  authMiddleware,
  asyncWrapper(loadsController.getShippingInfo),
);
module.exports = {
  loadsRouter: router,
};
