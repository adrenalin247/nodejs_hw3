const express = require('express');

const router = express.Router();

const { asyncWrapper } = require('../utils');

const authController = require('../controllers/authController');

router.post('/register', asyncWrapper(authController.registerUser));
router.post('/login', asyncWrapper(authController.loginUser));
router.post('/forgot_password', asyncWrapper(authController.forgot_password));

module.exports = {
  authRouter: router,
};
