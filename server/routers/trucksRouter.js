const express = require('express');

const router = express.Router();

const { asyncWrapper } = require('../utils');
const { authMiddleware } = require('../middleware/authMiddleware');

const trucksController = require('../controllers/trucksController');

router.get('/', authMiddleware, asyncWrapper(trucksController.getAllTrucks));
router.post('/', authMiddleware, asyncWrapper(trucksController.addTruck));
router.get('/:id', authMiddleware, asyncWrapper(trucksController.getTruck));

router.put(
  '/:id',
  authMiddleware,
  asyncWrapper(trucksController.changeTruckType),
);

router.delete(
  '/:id',
  authMiddleware,
  asyncWrapper(trucksController.deleteTruck),
);
router.post(
  '/:id/assign',
  authMiddleware,
  asyncWrapper(trucksController.assignTruck),
);

module.exports = {
  trucksRouter: router,
};
