const Joi = require("joi");
const joiLoad = require("./joiLoads");
const joiUsers = require("../../Users/Joi/joiUsers");
const userData = require("../../Users/data");
const loadData = require("../data");

const addLoad = async ({ created_by, ...loadBody }) =>
  joiLoad.created_by
    .concat(joiLoad.body)
    .validateAsync({ ...loadBody, created_by });

const getAllLoads = async ({ status, limit, offset, role, userId }) => {
  const owner =
    role === userData.roles.shipper
      ? { created_by: userId }
      : { assigned_to: userId };

  return joiUsers.role
    .concat(joiLoad.created_by)
    .concat(joiLoad.assigned_to)
    .concat(joiLoad.allLoadsQueries)
    .xor("assigned_to", "created_by")
    .validateAsync({
      status,
      limit,
      offset,
      role,
      ...owner,
    });
};

const getActiveLoad = async ({ role, userId }) =>
  joiUsers.driver
    .concat(joiLoad.assigned_to)
    .concat(
      joiLoad.status
        .keys({
          status: Joi.string().default(
            Object.values(loadData.statusesForDriver)
          ),
        })
        .required()
    )
    .required()
    .validateAsync({ role, assigned_to: userId });

// eslint-disable-next-line max-len
const getLoadById = async ({ loadId, userId }) =>
  joiLoad.loadId.concat(joiUsers.userId).validateAsync({
    loadId,
    userId,
  });

const editLoad = async ({ loadId, role, userId, ...loadBody }) =>
  joiUsers.shipper
    .concat(joiLoad.created_by)
    .concat(joiLoad.body)
    .concat(joiLoad.loadId)
    .validateAsync({
      ...loadBody,
      role,
      loadId,
      created_by: userId,
    });

const deleteLoad = async ({ loadId, role, userId }) =>
  joiUsers.shipper
    .concat(joiLoad.created_by)
    .concat(joiLoad.loadId)
    .validateAsync({ role, loadId, created_by: userId });

const loadToPost = async ({ loadId, role, userId }) =>
  joiUsers.shipper
    .concat(joiLoad.created_by)
    .concat(joiLoad.loadId)
    .concat(
      joiLoad.status.keys({
        status: Joi.string().default(loadData.statuses.NEW),
      })
    )
    .validateAsync({ role, loadId, created_by: userId });

const shippingInfo = async ({ loadId, role, userId }) =>
  joiUsers.user
    .concat(joiUsers.userId)
    .concat(joiLoad.loadId)
    .validateAsync({ role, loadId, userId });

module.exports = {
  addLoad,
  getAllLoads,
  getActiveLoad,
  getLoadById,
  editLoad,
  deleteLoad,
  loadToPost,
  shippingInfo,
};
