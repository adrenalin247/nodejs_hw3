const Joi = require('joi');
const { joiObjectId } = require('../../joiUtils');

const userData = require('../../Users/data');
const loadData = require('../data');

const body = Joi.object({
  name: Joi.string().trim(true).required(),
  payload: Joi.number().required(),
  pickup_address: Joi.string().trim(true).required(),
  delivery_address: Joi.string().trim(true).required(),
  dimensions: Joi.object({
    width: Joi.number().required(),
    length: Joi.number().required(),
    height: Joi.number().required(),
  }).required(),
});

const created_by = Joi.object({
  created_by: joiObjectId().trim(),
});

const loadId = Joi.object({
  loadId: joiObjectId().trim(),
});

const assigned_to = Joi.object({
  assigned_to: joiObjectId().trim(),
});

const status = Joi.object({
  status: Joi.string()
    .when('role', {
      is: userData.roles.driver,
      then: Joi.valid(...loadData.statusesForDriver),
    })
    .when('role', {
      is: userData.roles.shipper,
      then: Joi.valid(...Object.values(loadData.statuses)),
    })
    .trim(),
});

const allLoadsQueries = status.keys({
  limit: Joi.number().max(50).default(10),
  offset: Joi.number().default(0),
});

module.exports = {
  body,
  created_by,
  assigned_to,
  allLoadsQueries,
  status,
  loadId,
};
