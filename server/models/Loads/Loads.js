const mongoose = require('mongoose');
const loadData = require('./data');

const LogsSchema = new mongoose.Schema({
  message: {
    type: String,
    required: true,
  },
  time: {
    type: Date,
    required: true,
    default: Date.now,
  },
});

const Loads = mongoose.model('Load', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
    ref: 'User',
    default: null,
  },
  status: {
    type: String,
    required: true,
    enum: Object.values(loadData.statuses),
    default: loadData.statuses.NEW,
  },
  state: {
    type: String,
    enum: Object.values(loadData.states),
    default: loadData.states.NONE,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: [LogsSchema],
  created_date: {
    type: Date,
    required: true,
    default: Date.now,
  },
});
module.exports = {
  Loads,
};
