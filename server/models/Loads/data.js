const statuses = {
  NEW: 'NEW',
  POSTED: 'POSTED',
  ASSIGNED: 'ASSIGNED',
  SHIPPED: 'SHIPPED',
};

const statusesForDriver = [
  statuses.POSTED,
  statuses.ASSIGNED,
  statuses.SHIPPED,
];

const states = {
  NONE: '',
  EN_ROUTE_TO_PICK_UP: 'En route to Pick Up',
  ARRIVED_TO_PICK_UP: 'Arrived to Pick Up',
  EN_ROUTE_TO_DELIVERY: 'En route to delivery',
  ARRIVED_TO_DELIVERY: 'Arrived to delivery',
};

module.exports = {
  states,
  statuses,
  statusesForDriver,
};
