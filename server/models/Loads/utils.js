const loadData = require('./data');

const getNextState = (currentState) => {
  const state = loadData.states;

  switch (currentState) {
    case state.NONE:
      return state.EN_ROUTE_TO_PICK_UP;
    case state.EN_ROUTE_TO_PICK_UP:
      return state.ARRIVED_TO_PICK_UP;
    case state.ARRIVED_TO_PICK_UP:
      return state.EN_ROUTE_TO_DELIVERY;
    default:
      return state.ARRIVED_TO_DELIVERY;
  }
};

module.exports = {
  getNextState,
};
