const Joi = require('joi');
const { roles } = require('../data');
const joiUtils = require('../../joiUtils');

const userId = Joi.object({
  userId: joiUtils.joiObjectId().required(),
});

const role = Joi.object({
  role: Joi.string().valid(roles.shipper, roles.driver).required(),
});

const driver = Joi.object({
  role: Joi.string().valid(roles.driver).required(),
});
const shipper = Joi.object({
  role: Joi.string().valid(roles.shipper).required(),
});

const email = Joi.object({
  email: Joi.string().email().trim(true).required(),
});

const password = Joi.object({
  // password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).required(),
  password: Joi.string().pattern(/^[a-zA-Z0-9]{3,30}$/).required(),
});

const register = email.concat(password).concat(role);

const login = email.concat(password);
const assignTruck = userId.concat(driver);

module.exports = {
  userId,
  email,
  password,
  role,
  driver,
  shipper,
  register,
  login,
  assignTruck,
};
