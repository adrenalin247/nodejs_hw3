const joiUser = require('./joiUsers');
const { message } = require('../../../utils');

async function roleDriver(role, res) {
  return joiUser.driver
    .validateAsync({ role })
    .catch((err) => res.status(400).json(message(err.message)));
}

async function roleShipper(role, res) {
  return joiUser.shipper
    .validateAsync({ role })
    .catch((err) => res.status(400).json(message(err.message)));
}

async function roles(role, res) {
  return joiUser.role
    .validateAsync({ role })
    .catch((err) => res.status(400).json(message(err.message)));
}

module.exports = {
  roleDriver,
  roleShipper,
  roles,
};
