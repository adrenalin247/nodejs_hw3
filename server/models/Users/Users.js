const mongoose = require('mongoose');

const User = mongoose.model('User', {
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
  created_by: {
    type: Date,
    required: true,
    default: Date.now,
  },
});

module.exports = {
  User,
};
