const roles = {
  shipper: 'SHIPPER',
  driver: 'DRIVER',
};

module.exports = {
  roles,
};
