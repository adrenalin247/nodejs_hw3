const Joi = require('joi');

const joiObjectId = () => Joi.string().hex().length(24);

module.exports = {
  joiObjectId,
};
