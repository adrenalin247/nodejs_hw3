const Joi = require('joi');

const truckUtils = require('../utils');
const joiUtils = require('../../joiUtils');
const truckData = require('../data');

const truckId = Joi.object({
  truckId: joiUtils.joiObjectId().required(),
});

const assigned_to = Joi.object({
  assigned_to: joiUtils.joiObjectId().required(),
});

const type = Joi.object({
  type: Joi.string()
    .valid(...truckUtils.getTruckTypes())
    .required(),
});

const createTruck = type.keys({
  created_by: joiUtils.joiObjectId().required(),
});

const status = type.keys({
  status: Joi.string().valid(...Object.values(truckData.statuses)),
});

const availableCondition = Joi.object({
  status: Joi.string()
    .valid(truckData.statuses.IS)
    .default(truckData.statuses.IS),
});

const changeTruck = truckId.concat(type);

module.exports = {
  createTruck,
  changeTruck,
  truckId,
  assigned_to,
  type,
  status,
  availableCondition,
};
