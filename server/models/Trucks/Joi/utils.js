const Joi = require('joi');
const joiTrucks = require('./joiTrucks');
const truckData = require('../data');
const { message } = require('../../../utils');

async function truckId(id, res) {
  return joiTrucks.truckId
    .validateAsync({ truckId: id })
    .catch((err) => res.status(400).json(message(err.message)));
}

async function assignTruck(id, created_by) {
  return joiTrucks.truckId
    .concat(joiTrucks.assigned_to)
    .validateAsync({ truckId: id, assigned_to: created_by });
}

async function availableTrucks(loadDimensions, payload) {
  const validTrucks = truckData.trucks.filter((el) => {
    const isSizeValid = loadDimensions.width < el.dimensions.width
      && loadDimensions.height < el.dimensions.height
      && loadDimensions.length < el.dimensions.length;

    const isPayloadValid = payload < el.payload;

    return isSizeValid && isPayloadValid;
  });
  const validTrucksType = validTrucks.map((el) => el.type);

  return joiTrucks.availableCondition
    .keys({
      type: Joi.array()
        .items(Joi.string().valid(...validTrucksType))
        .default(validTrucksType),
      assigned_to: Joi.object().default({
        $ne: null,
      }),
    })
    .validateAsync({});
}

module.exports = {
  truckId,
  assignTruck,
  availableTrucks,
};
