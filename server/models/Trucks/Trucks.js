const mongoose = require('mongoose');

const truckData = require('./data');
const truckUtils = require('./utils');

const TruckSchema = new mongoose.Schema({
  created_by: {
    type: mongoose.ObjectId,
    required: true,
    ref: 'User',
  },
  assigned_to: {
    type: mongoose.ObjectId,
    required: false,
    default: null,
    ref: 'User',
  },
  type: {
    type: String,
    required: true,
    enum: truckUtils.getTruckTypes(),
  },
  status: {
    type: String,
    required: true,
    enum: truckUtils.getTruckStatuses(),
    default: truckData.statuses.IS,
  },
  created_date: {
    type: Date,
    required: true,
    default: Date.now,
  },
});

const Truck = mongoose.model('truck', TruckSchema);

module.exports = {
  Truck,
};
