const truckData = require('./data');

const getTruckTypes = () => truckData.trucks.map((el) => el.type);

const getTruckStatuses = () => Object.values(truckData.statuses).map((el) => el);

function formatTruck(truck) {
  return {
    _id: truck._id,
    created_by: truck.created_by,
    assigned_to: truck.assigned_to,
    type: truck.type,
    status: truck.status,
    created_date: truck.created_date,
  };
}

function formatTrucks(trucks) {
  return trucks.map((el) => formatTruck(el));
}

module.exports = {
  getTruckStatuses,
  getTruckTypes,
  formatTruck,
  formatTrucks,
};
