function errorUserNotFound() {
  throw new Error('User not valid');
}

function errorLoadNotFound() {
  throw new Error('Load not valid');
}

module.exports = {
  errorUserNotFound,
  errorLoadNotFound,
};
