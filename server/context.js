const user_not_found = 'User not Found';
const load_not_found = 'Load not Found';

module.exports = {
  user_not_found,
  load_not_found,
};
