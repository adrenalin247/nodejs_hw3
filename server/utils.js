const asyncWrapper = (controller) => (req, res, next) => {
  controller(req, res, next).catch(next);
};

function random(min, max) {
  const maxUpperLimit = 1;

  return Math.floor(Math.random() * (max - min + maxUpperLimit) + min);
}

function message(messageTxt) {
  return {
    message: messageTxt,
  };
}

module.exports = {
  message,
  random,
  asyncWrapper,
};
