const joiUserUtils = require('../models/Users/Joi/utils');
const joiTruck = require('../models/Trucks/Joi/joiTrucks');
const joiTruckUtils = require('../models/Trucks/Joi/utils');
const trucksService = require('../services/trucksService');
const truckUtils = require('../models/Trucks/utils');
const { message } = require('../utils');

const getAllTrucks = async (req, res, next) => {
  const role = joiUserUtils.roleDriver(req.user.role, res);
  const userId = req.user.id;

  return (
    !role.statusCode
    && trucksService
      .getTrucks({ userId })
      .then((trucks) => res.json({
        trucks: truckUtils.formatTrucks(trucks),
      }))
      .catch((err) => next(err.message))
  );
};

const addTruck = async (req, res, next) => {
  const role = joiUserUtils.roleDriver(req.user.role, res);
  const truck = !role.statusCode
    && (await joiTruck.createTruck
      .validateAsync({ ...req.body, created_by: req.user.id })
      .catch((err) => res.status(400).json(message(err.message))));

  return (
    !truck.statusCode
    && trucksService
      .saveTruck(truck)
      .then(() => res.json(message('Truck created successfully')))
      .catch((err) => next(err.message))
  );
};

const getTruck = async (req, res, next) => {
  const role = joiUserUtils.roleDriver(req.user.role, res);
  const truckProps = !role.statusCode && (await joiTruckUtils.truckId(req.params.id, res));

  return (
    !truckProps.statusCode
    && trucksService
      .getTruckById(truckProps.truckId)
      .then((truck) => res.json({
        truck: truckUtils.formatTruck(truck),
      }))
      .catch((err) => next(err.message))
  );
};

const changeTruckType = async (req, res, next) => {
  const truckId = req.params.id;
  const role = joiUserUtils.roleDriver(req.user.role, res);
  const newTruck = !role.statusCode
    && (await joiTruck.changeTruck
      .validateAsync({ ...req.body, truckId })
      .catch((err) => res.status(400).json(message(err.message))));

  return (
    !newTruck.statusCode
    && trucksService
      .changeTruckTypeById(newTruck)
      .then(() => res.json(message('Truck details changed successfully')))
      .catch((err) => next(err.message))
  );
};

const deleteTruck = async (req, res, next) => {
  const role = joiUserUtils.roleDriver(req.user.role, res);
  const truckId = !role.statusCode && (await joiTruckUtils.truckId(req.params.id, res));

  return (
    !truckId.statusCode
    && trucksService
      .deleteTruckById(truckId)
      .then((result) => {
        if (!result) {
          return res.json(message('Truck was not deleted'));
        }
        return res.json(message('Truck deleted successfully'));
      })
      .catch((err) => next(err.message))
  );
};

const assignTruck = async (req, res, next) => {
  const truckId = req.params.id;
  const userRole = joiUserUtils.roleDriver(req.user.role, res);
  const truck = !userRole.statusCode
    && (await joiTruckUtils
      .assignTruck(truckId, req.user.id)
      .catch((err) => res.status(400).json(message(err.message))));

  return (
    !truck.statusCode
    && trucksService
      .assignTruckById(truck)
      .then(() => res.json(message('Truck assigned successfully')))
      .catch((err) => next(err.message))
  );
};

module.exports = {
  getAllTrucks,
  addTruck,
  getTruck,
  changeTruckType,
  deleteTruck,
  assignTruck,
};
