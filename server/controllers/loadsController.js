const { message } = require("../utils");
const context = require("../context");
const loadData = require("../models/Loads/data");
const truckData = require("../models/Trucks/data");
const joiLoadUtils = require("../models/Loads/Joi/utils");
const joiTruckUtils = require("../models/Trucks/Joi/utils");
const joiUserUtils = require("../models/Users/Joi/utils");
const loadsService = require("../services/loadsService");
const truckService = require("../services/trucksService");

const getAllLoads = async (req, res, next) => {
  const data = await joiLoadUtils
    .getAllLoads({
      ...req.query,
      role: req.user.role,
      userId: req.user.id,
    })
    .catch((err) => res.status(400).json(message(err.message)));

  return (
    !data.statusCode &&
    loadsService
      .getLoads(data)
      .then((loads) =>
        res.json({
          loads,
        })
      )
      .catch((err) => next(err.message))
  );
};

const addLoad = async (req, res, next) => {
  const role = joiUserUtils.roleShipper(req.user.role, res);
  const load =
    !role.statusCode &&
    (await joiLoadUtils
      .addLoad({
        ...req.body,
        created_by: req.user.id,
      })
      .catch((err) => res.status(400).json(message(err.message))));

  return (
    !load.statusCode &&
    loadsService
      .saveLoad(load)
      .then(() => res.json(message("Load created successfully")))
      .catch((err) => next(err.message))
  );
};

const getActiveLoad = async (req, res, next) => {
  const data = await joiLoadUtils
    .getActiveLoad({ role: req.user.role, userId: req.user.id })
    .catch((err) => res.status(400).json(message(err.message)));

  return (
    !data.statusCode &&
    loadsService
      .getActive(data)
      .then((activeLoads) => {
        const { __v, ...result } = activeLoads.toObject();
        return res.status(200).json({
          load: { ...result },
        });
      })
      .catch((err) => next(err.message))
  );
};

const iterateActiveLoadState = async (req, res, next) => {
  const data = await joiLoadUtils
    .getActiveLoad({ role: req.user.role, userId: req.user.id })
    .catch((err) => res.status(400).json(message(err.message)));

  const load =
    !data.statusCode &&
    (await loadsService.iterateActiveLoadState(data).catch((err) => {
      next(err.message);
      return { error: "Error: Load Iterate" };
    }));

  if (load?.error) return;

  if (load.state === loadData.states.ARRIVED_TO_DELIVERY) {
    await truckService.finishDelivery({ assigned_to: load.assigned_to });
    load.status = loadData.statuses.SHIPPED;
    load.save();
  }

  res.status(200).json(message(`Load state changed to ${load.state}`));
};

const getLoad = async (req, res, next) => {
  const data = await joiLoadUtils
    .getLoadById({ loadId: req.params.id, userId: req.user.id })
    .catch((err) => res.status(400).json(message(err.message)));

  return (
    !data.statusCode &&
    loadsService
      .getLoadById(data)
      .then((load) =>
        res.json({
          load,
        })
      )
      .catch((err) => next(err.message))
  );
};

const editLoad = async (req, res, next) => {
  const { role } = req.user;
  const loadId = req.params.id;

  const data = await joiLoadUtils
    .editLoad({
      ...req.body,
      role,
      loadId,
      userId: req.user.id,
    })
    .catch((err) => res.status(400).json(message(err.message)));

  return (
    !data.statusCode &&
    loadsService
      .editLoadById(data)
      .then(() => res.json(message("Load details changed successfully")))
      .catch((err) => next(err.message))
  );
};

const deleteLoad = async (req, res, next) => {
  const load = await joiLoadUtils
    .deleteLoad({
      role: req.user.role,
      loadId: req.params.id,
      userId: req.user.id,
    })
    .catch((err) => res.status(400).json(message(err.message)));

  if (!load.statusCode) {
    loadsService
      .deleteLoadById({ ...load })
      .then(() => res.json(message("Load deleted successfully")))
      .catch((err) => next(err.message));
  }
};

const postLoad = async (req, res, next) => {
  const { role, id: userId } = req.user;
  const loadId = req.params.id;

  const loadToPost = await joiLoadUtils
    .loadToPost({ role, loadId, userId })
    .catch((err) => res.status(400).json(message(err.message)));

  const load =
    !loadToPost?.statusCode &&
    (await loadsService
      .getCreatorsLoadById(loadToPost)
      .catch((err) => next(err.message)));

  if (!load) {
    return next(context.load_not_found);
  }

  load.status = loadData.statuses.POSTED;
  const postedLoad = await load.save().catch((err) => {
    next(err.message);
    return { error: err };
  });
  if (postedLoad?.error) return null;

  const validTrucks = await joiTruckUtils
    .availableTrucks(postedLoad.dimensions, postedLoad.payload)
    .catch((err) => {
      const validTruckError = `Valid truck search error ${err.message}`;
      next(validTruckError);
      return { error: `Valid truck search error ${err.message}` };
    });

  const availableTruck =
    !validTrucks?.error &&
    (await truckService.availableTruck(validTrucks).catch(() => {
      const availableTruckError = "Error during available truck search";
      next(availableTruckError);
      return { error: availableTruckError };
    }));

  const driver_found = !!(!availableTruck?.error && availableTruck);
  if (driver_found) {
    postedLoad.status = loadData.statuses.ASSIGNED;
    postedLoad.assigned_to = availableTruck.assigned_to;
    await postedLoad.save();
    await loadsService.iterateActiveLoadState({
      assigned_to: postedLoad.assigned_to,
      status: postedLoad.status,
    });

    availableTruck.status = truckData.statuses.OL;
    await availableTruck.save();

    return res.json({
      ...message("Load posted successfully"),
      driver_found,
    });
  }

  postedLoad.status = loadData.statuses.NEW;
  await postedLoad.save();

  return res.json({
    ...message("Load posted unsuccessfully"),
    driver_found,
  });
};

const getShippingInfo = async (req, res) => {
  const { role, id: userId } = req.user;
  const loadId = req.params.id;

  const data = await joiLoadUtils
    .shippingInfo({ role, loadId, userId })
    .catch((err) => res.status(400).json(message(err.message)));

  const load = !data.statusCode && (await loadsService.getLoadById(data));

  const truck =
    load &&
    (await truckService.getTruck({
      assigned_to: String(load.assigned_to),
    }));

  if (!load || !truck) {
    return res.status(400).json(message(context.load_not_found));
  }
  const { __v, ...result } = load;
  return (
    truck &&
    res.status(200).json({
      load: { ...result },
      truck,
    })
  );
};

module.exports = {
  addLoad,
  getAllLoads,
  getActiveLoad,
  iterateActiveLoadState,
  getLoad,
  deleteLoad,
  postLoad,
  getShippingInfo,
  editLoad,
};
