const { message } = require("../utils");

const joiUser = require("../models/Users/Joi/joiUsers");
const authService = require("../services/authService");

const registerUser = async (req, res, next) => {
  const userData = await joiUser.register
    .validateAsync({ ...req.body })
    .catch((err) => res.status(400).json(message(err.message)));

  return (
    !userData.statusCode &&
    authService
      .saveUser({ ...userData })
      .then(() => res.status(200).json(message("Profile created successfully")))
      .catch((err) => next(err.message))
  );
};

const loginUser = async (req, res, next) => {
  const userData = await joiUser.login
    .validateAsync({ ...req.body })
    .catch((err) => res.status(400).json(message(err.message)));

  return (
    !userData.statusCode &&
    authService
      .getToken({ ...userData })
      .then((token) => {
        res.json(token);
      })
      .catch((err) => next(err.message))
  );
};

const forgot_password = async (req, res, next) => {
  const userData = await joiUser.email
    .validateAsync({ ...req.body })
    .catch((err) => res.status(400).json(message(err.message)));

  return (
    !userData.statusCode &&
    authService
      .setNewPassword({ ...userData })
      .then(() => {
        res.json(message("New password sent to your email address"));
      })
      .catch((err) => next(err.message))
  );
};

module.exports = {
  registerUser,
  loginUser,
  forgot_password,
};
