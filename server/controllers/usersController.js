require('dotenv').config();
const bcryptjs = require('bcryptjs');

const { message } = require('../utils');
const errors = require('../errors');
const joiUser = require('../models/Users/Joi/joiUsers');
const userService = require('../services/usersService');

const getAuthenticatedUser = async (req, res, next) => {
  const { email } = req.user;
  return userService
    .getUser(email)
    .then((user) => (user
      ? res.json({
        user: {
          _id: user._id,
          role: user.role,
          email: user.email,
          created_by: user.created_by,
        },
      })
      : errors.errorUserNotFound()))
    .catch((err) => next(err.message));
};

const deleteAuthenticatedUser = async (req, res, next) => userService
  .deleteUser(req.user.id)
  .then(() => res.json(message('Profile deleted successfully')))
  .catch((err) => next(err.message));

const changeUserPassword = async (req, res, next) => {
  const { oldPassword, newPassword } = req.body;
  const { user } = req;

  const userData = await userService
    .getUser(user.email)
    .then((userInfo) => userInfo || errors.errorUserNotFound())
    .catch((err) => {
      next(err.message);
      return null;
    });

  const password = userData
    && (await joiUser.password
      .validateAsync({ password: newPassword })
      .catch((err) => res.status(400).json(message(err.message))));

  const isOldPasswordValid = !password.statusCode
    && (await bcryptjs.compare(String(oldPassword), String(userData.password)));

  if (!isOldPasswordValid) {
    return res.status(400).json(message('Old password is not valid'));
  }

  return userService
    .changePassword(user.id, newPassword)
    .then(() => res.json(message('Password changed successfully')))
    .catch((err) => next(err.message));
};

module.exports = {
  getAuthenticatedUser,
  deleteAuthenticatedUser,
  changeUserPassword,
};
