# EPAM NodeJS HW3 2022 

UBER like service for freight trucks, in REST style, using MongoDB as database. This service should help regular people to deliver their stuff and help drivers to find loads and earn some money. Application contains 2 roles, driver and shipper. 

## Prerequisite
Node.js and MongoDB installed on your work station 

## To interact with API
Vist https://editor.swagger.io/


import `openapi.yaml` file

## Available Scripts

In the project directory, you can run:

### `npm install`

To install app server files

### `npm start`

Start server at [http://localhost:8080](http://localhost:8080)

### `npm test`

Launches the test 

